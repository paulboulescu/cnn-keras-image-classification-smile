
from keras.layers import Input, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D
from keras.layers import AveragePooling2D, MaxPooling2D, Dropout, GlobalMaxPooling2D, GlobalAveragePooling2D
from keras.models import Model
import keras.backend as K
from utils import *


def model(input_shape):

    """
    Arguments:
    input_shape -- shape of the images of the dataset

    Returns:
    model -- a Model() instance in Keras

    """

    # CONV -> BN -> RELU -> MAxPOOL -> CONV -> BN -> RELU -> MAxPOOL -> CONV -> BN -> RELU -> MAxPOOL
    # -> FLATTEN + FULLyCONNECTED -> SIGMOD

    # Define the input placeholder as a tensor with shape input_shape
    x_input = Input(input_shape)

    # Zero-Padding: pads the border of x_input with zeroes
    x = ZeroPadding2D((3, 3))(x_input)

    # CONV -> BN -> RELU
    x = Conv2D(32, (3, 3), strides=(1, 1), name='conv0')(x)
    x = BatchNormalization(axis=3, name='bn0')(x)
    x = Activation('relu')(x)

    # MAxPOOL
    x = MaxPooling2D((2, 2), name='max_pool0')(x)

    # CONV -> BN -> RELU
    x = Conv2D(32, (3, 3), strides=(1, 1), name='conv1')(x)
    x = BatchNormalization(axis=3, name='bn1')(x)
    x = Activation('relu')(x)

    # MAxPOOL
    x = MaxPooling2D((2, 2), name='max_pool1')(x)

    # CONV -> BN -> RELU
    x = Conv2D(32, (3, 3), strides=(1, 1), name='conv2')(x)
    x = BatchNormalization(axis=3, name='bn2')(x)
    x = Activation('relu')(x)

    # MAxPOOL
    x = MaxPooling2D((2, 2), name='max_poo2')(x)

    # FLATTEN + FULLyCONNECTED
    x = Flatten()(x)
    x = Dense(1, activation='sigmoid', name='fc')(x)

    # Create model. This creates your Keras model instance, used to train/test the model.
    model = Model(inputs=x_input, outputs=x, name='model')

    return model


K.set_image_data_format('channels_last')

# Loading the dataset
x_train_orig, y_train_orig, x_test_orig, y_test_orig, classes = load_dataset("datasets/train_happy.h5",
                                                                             "datasets/test_happy.h5")

# Normalize image vectors
x_train = x_train_orig/255.
x_test = x_test_orig/255.

# Reshape
y_train = y_train_orig.T
y_test = y_test_orig.T

model = model((64,64,3))
model.compile(optimizer="Adam", loss="binary_crossentropy", metrics=["accuracy"])
model.fit(x = x_train, y = y_train, epochs = 40, batch_size = 16)
preds = model.evaluate(x = x_test, y = y_test)

print("Loss = " + str(preds[0]))
print("Test Accuracy = " + str(preds[1]))
