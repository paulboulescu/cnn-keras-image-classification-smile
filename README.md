# CNN Image Classifier with Keras

## About
Convolutional Neural Network model with Keras, used to classify images of smiling and not smiling people.

## Instalation
1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/cnn-keras-image-classification-smile.git
```

2. Train and Test
```
$ python model.py
```

## Requirements
1. Keras

## Details
* Model: CONV -> BN -> RELU -> MAxPOOL -> CONV -> BN -> RELU -> MAxPOOL -> CONV -> BN -> RELU -> MAxPOOL -> FLATTEN + FULLyCONNECTED -> SIGMOD
* Train Accuracy: **1.0**
* Test Accuracy: **0.966666**
* Epochs: **40**
* MiniBatch: **16**
* Learning Rate: **0.001**
* Optimizer: **Adam**
* Initializer: **Xavier**
* Loss: **Softmax / Cross Entropy**

## Disclaimer
Created under the Deep Learning Specialization (Andrew Ng, Younes Bensouda Mourri, Kian Katanforoosh) - Coursera. The code uses ideas, datasets, algorithms, and code fragments presented in the Course.